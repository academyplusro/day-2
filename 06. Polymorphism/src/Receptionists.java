
public class Receptionists extends EmployeeSalaries{
	
	private String name;
	private String gender;
	private long  salary ;
	
	public long getSalary() {
		return salary;
	}

	public void setSalary(long salary) {
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	
	
	public Receptionists(String name, String gender,long salary){
		this.name=name;
		this.gender=gender;
		this.salary = salary;
	}

	//Note that once again, the CalculateSalary method has been overridden
	@Override
    public void CalculateSalary() {
		
		
		System.out.println("The salary of Receptionists is somewhere around  "+getSalary());
		
		}

	
}
