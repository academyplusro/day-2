
public class Hotel_Bedrooms implements MyInterface {

	
	private int dimension;
	private int height;
	private int weight;
	


	public int getDimension() {
		return dimension;
	}
	public void setDimension(int dimension) {
		this.dimension = dimension;
	}
	
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public Hotel_Bedrooms( int dimension ,int height, int weight) {
		super();
		this.dimension = dimension;
		this.height = height;
		this.weight = weight;
		
	}
	
	//we override the two methods from interface
	@Override
	public void CalculateSize() {
		
		System.out.println(getDimension()+getHeight()+getWeight());
		
		}
	
	public void getStyle() {
		System.out.println("The style of this bed is called Headboard and Side Rails ");
		
	}
	 
	
	
}
